<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('professionals')->group(function() {
	Route::delete('/{id}', 'UserController@destroy')->name('professionals.delete');
	Route::put('/change-lock-professional/{id}', 'UserController@changeLockProfessional')->name('professionals.change-lock-professional');
});

Route::prefix('services')->group(function() {
	Route::delete('/{id}', 'ServiceController@destroy')->name('services.delete');
});

Route::prefix('subsidiaries')->group(function() {
	Route::delete('/{id}', 'SubsidiaryController@destroy')->name('subsidiaries.delete');
});

Route::prefix('calendars')->group(function() {
	Route::get('/dates', 'CalendarController@dates')->name('calendars.dates');
	Route::get('/check-availability/{date}/{professional_id}/{subsidiary_id}', 'CalendarController@checkAvailability')->name('calendars.available-blocks');
	Route::delete('/{id}', 'CalendarController@destroy')->name('calendars.delete');
});