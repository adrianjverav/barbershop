<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return redirect('login');
})->middleware('auth');*/

Auth::routes();

Route::middleware('auth')->group(function() {

	Route::get('/', function () {
	    return redirect('login');
	});

	Route::prefix('users')->group(function() {
		Route::get('/change-password', 'UserController@changePassword')->name('users.change-password');
		Route::put('/change-password', 'UserController@storeNewPassword')->name('users.store-new-password');
	});

	Route::get('/home', 'HomeController@index')->name('home');

	Route::get('clients', 'CalendarController@clients')->name('clients');

	Route::prefix('professionals')->group(function() {
		Route::get('/', 'UserController@index')->name('professionals.index');
		Route::get('/create', 'UserController@create')->name('professionals.create');
		Route::post('/create', 'UserController@store')->name('professionals.store');
		Route::get('/edit/{id}', 'UserController@edit')->name('professionals.edit');
		Route::put('/edit/{id}', 'UserController@update')->name('professionals.update');
	});

	Route::prefix('services')->group(function() {
		Route::get('/', 'ServiceController@index')->name('services.index');
		Route::get('/create', 'ServiceController@create')->name('services.create');
		Route::post('/create', 'ServiceController@store')->name('services.store');
		Route::get('/edit/{id}', 'ServiceController@edit')->name('services.edit');
		Route::put('/edit/{id}', 'ServiceController@update')->name('services.update');
	});

	Route::prefix('calendars')->group(function() {
		Route::get('/', 'CalendarController@index')->name('calendars.index');
		Route::get('/create', 'CalendarController@create')->name('calendars.create');
		Route::post('/create', 'CalendarController@store')->name('calendars.store');
		Route::get('/edit/{id}', 'CalendarController@edit')->name('calendars.edit');
		Route::put('/edit/{id}', 'CalendarController@update')->name('calendars.update');
	});

	Route::prefix('subsidiaries')->group(function() {
		Route::get('/', 'SubsidiaryController@index')->name('subsidiaries.index');
		Route::get('/create', 'SubsidiaryController@create')->name('subsidiaries.create');
		Route::post('/create', 'SubsidiaryController@store')->name('subsidiaries.store');
		Route::get('/edit/{id}', 'SubsidiaryController@edit')->name('subsidiaries.edit');
		Route::put('/edit/{id}', 'SubsidiaryController@update')->name('subsidiaries.update');
	});
});

