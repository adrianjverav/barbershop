<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subsidiary extends Model
{
    protected $table = 'subsidiaries';
    protected $fillable = ['location'];

    // Relaciones
    public function blocks()
    {
    	return $this->hasMany('App\Block');
    }
}
