<?php

namespace App\Http\Controllers;

use App\Subsidiary;
// use App\Http\Requests\subsidiarytore;
use Illuminate\Http\Request;

class SubsidiaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subsidiaries = Subsidiary::orderBy('location')->get();
        return view('admin.subsidiaries.index', ['subsidiaries' => $subsidiaries]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('admin.subsidiaries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subsidiary = new Subsidiary;
        $subsidiary->location = $request->location;
        $subsidiary->save();
        return redirect()->route('subsidiaries.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subsidiary = Subsidiary::find($id);
        $subsidiary->location = $request->location;
        $subsidiary->save();
        return redirect()->route('subsidiaries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subsidiary = Subsidiary::find($id);
        $subsidiary->delete();
        return response()->json(['result' => 'La sede fue removida exitosamente']);
    }
}
