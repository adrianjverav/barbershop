<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UsersStore;
use App\Http\Requests\UsersUpdate;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professionals = User::where('type', 'professional')->orderBy('name')->get();
        return view('admin.professionals.index', ['professionals' => $professionals]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.professionals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersStore $request)
    {
        $professional = new User;
        $professional->name = $request->name;
        $professional->surname = $request->surname;
        $professional->phone = $request->phone;
        $professional->type = 'professional';
        $professional->locked = false;
        $professional->email = $request->email;
        $professional->password = bcrypt($request->password);
        $professional->save();
        return redirect()->route('professionals.index');
    }

    public function changePassword()
    {
        return view('change-password');
    }

    public function storeNewPassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required'
        ]);

        $user = User::find(Auth::user()->id);
        $user->password = bcrypt($request->password);
        $user->save();
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $professional = User::find($id);
        return view('admin.professionals.edit', ['professional' => $professional]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsersUpdate $request, $id)
    {
        $professional = User::find($id);
        $professional->name = $request->name;
        $professional->surname = $request->surname;
        $professional->phone = $request->phone;
        $professional->email = $request->email;
        $professional->save();
        return redirect()->route('professionals.index');
    }

    // Cambiar el bloqueo de un profesional
    public function changeLockProfessional($id)
    {
        $professional = User::find($id);
        $professional->locked = !$professional->locked;
        $professional->save();
        $result = (!$professional->locked) ? 'desbloqueado' : 'bloqueado';
        return response()->json(['result' => "El profesional ha sido " . $result]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $professional = User::find($id);
        $professional->delete();
        return response()->json(['result' => 'El profesional ha sido removido exitosamente.']);
    }
}
