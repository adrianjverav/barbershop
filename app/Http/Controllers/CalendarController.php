<?php

namespace App\Http\Controllers;

use App\Calendar;
use App\Subsidiary;
use App\Block;
use App\Service;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests\StoreCalendar;
use Illuminate\Support\Facades\DB;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.calendars.index');
    }

    // Crea el json adecuado para mostrar las citas en el calendario
    public function dates()
    {
        $calendars = DB::table('calendar_service')->get();
        $events = collect([]);
        foreach ($calendars as $calendar) {
            $professional = User::find($calendar->professional_id);
            $date = Calendar::find($calendar->calendar_id)->date;
            $service = Service::find($calendar->service_id)->service;
            $subsidiary = Subsidiary::find($calendar->subsidiary_id)->location;
            $events->push([
                'date' => $date,
                'title' => formatterBlock($calendar->start_block) . '-' . formatterBlock($calendar->end_block) . ' | ' . $professional->name . ' ' . $professional->surname,
                'service' => $service,
                'client' => $calendar->client_name . ' ' . $calendar->client_surname,
                'client_phone' => $calendar->client_phone,
                'professional' => $professional->name . ' ' . $professional->surname,
                'blocks' => formatterBlock($calendar->start_block) . '-' . formatterBlock($calendar->end_block),
                'notes' => $calendar->notes,
                'subsidiary' => $subsidiary,
                'id' => $calendar->id
            ]);
        }
        echo json_encode($events);
    }

    // Muestro todos los clientes
    public function clients()
    {
        $clients = Calendar::getAllClients();
        return view('admin.calendars.clients', ['clients' => $clients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::all();
        $professionals = User::where('type', 'professional')->where('locked', false)->get();
        $subsidiaries = Subsidiary::all();
        return view('admin.calendars.create', ['services' => $services, 'professionals' => $professionals, 'subsidiaries' => $subsidiaries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCalendar $request)
    {
        $blocksOfTheService = Service::find($request->service_id);
        $calendar = Calendar::firstOrCreate(['date' => $request->date]);
        $blocks = ['1000', '1015', '1030', '1045', '1100', '1115', '1130', '1145', '1200',
                    '1215', '1230', '1245', '1300', '1315', '1330', '1345', '1400', '1415',
                    '1430', '1445', '1500', '1515', '1530', '1545', '1600', '1615', '1630',
                    '1645', '1700', '1715', '1730', '1745', '1800', '1815', '1830', '1845',
                    '1900', '1915', '1930', '1945', '2000'];
        $indexEndBlock = array_search($request->start_block, $blocks) + $blocksOfTheService->blocks;
        $endBlockService = $blocks[($indexEndBlock > count($blocks)) ? count($blocks) - 1 : $indexEndBlock];
        $calendar->services()->attach($request->service_id, [
            'client_name' => $request->client_name,
            'client_surname' => $request->client_surname,
            'zip_code' => $request->zip_code,
            'notes' => $request->notes,
            'client_phone' => $request->client_phone,
            'start_block' => $request->start_block,
            'end_block' => $endBlockService,
            'professional_id' => $request->professional_id,
            'subsidiary_id' => $request->subsidiary_id
        ]);
        $startBlock = array_search($request->start_block, $blocks);
        $endBlock = array_search($endBlockService, $blocks);
        $busyBlocks = array_slice($blocks, $startBlock, $endBlock - $startBlock);
        foreach ($busyBlocks as $block) {
            $newBlock = new Block;
            $newBlock->block = $block;
            $newBlock->calendar_id = $calendar->id;
            $newBlock->subsidiary_id = $request->subsidiary_id;
            $newBlock->save();
        }
        return redirect()->route('calendars.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function show(Calendar $calendar)
    {
        //
    }

    // Validar bloques disponibles para una fecha
    public function checkAvailability($date, $professionalId, $subsidiaryId)
    {
        $calendarId = Calendar::where('date', $date)->first(['id']);
        $blocks = collect(['1000', '1015', '1030', '1045', '1100', '1115', '1130', '1145', '1200',
                            '1215', '1230', '1245', '1300', '1315', '1330', '1345', '1400', '1415',
                            '1430', '1445', '1500', '1515', '1530', '1545', '1600', '1615', '1630',
                            '1645', '1700', '1715', '1730', '1745', '1800', '1815', '1830', '1845',
                            '1900', '1915', '1930', '1945', '2000']);
        if (!$calendarId) {
            return response()->json(['blocks' => $blocks]);
        }
        
        $calendarService = DB::table('calendar_service')
                            ->where('calendar_id', $calendarId->id)
                            ->where('professional_id', $professionalId)
                            ->where('subsidiary_id', $subsidiaryId)
                            ->get();

        $blocksOccupied = collect([]);
        if ($calendarService->isEmpty()) {
            return response()->json(['blocks' => $blocks]);
        } else {
            $calendar = Calendar::find($calendarId->id);
            foreach ($calendar->blocks->where('subsidiary_id', $subsidiaryId) as $block) {
                $blocksOccupied->push($block->block);
            }
            $blocksAvailable = $blocks->diff($blocksOccupied);
            $blocksAvailableReturn = collect([]);
            foreach ($blocksAvailable as $block) {
                $blocksAvailableReturn->push($block);
            }
            return response()->json(['blocks' => $blocksAvailableReturn]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $calendarService = DB::table('calendar_service')->where('id', $id)->first();
        $calendar = Calendar::find($calendarService->calendar_id);

        $blocks = collect(['1000', '1015', '1030', '1045', '1100', '1115', '1130', '1145', '1200',
                            '1215', '1230', '1245', '1300', '1315', '1330', '1345', '1400', '1415',
                            '1430', '1445', '1500', '1515', '1530', '1545', '1600', '1615', '1630',
                            '1645', '1700', '1715', '1730', '1745', '1800', '1815', '1830', '1845',
                            '1900', '1915', '1930', '1945', '2000']);
        $blocksOccupied = collect([]);
        foreach ($calendar->blocks->where('subsidiary_id', $calendarService->subsidiary_id) as $block) {
            $blocksOccupied->push($block->block);
        }
        $blocksAvailable = $blocks->diff($blocksOccupied);
        $services = Service::all();
        $professionals = User::where('type', 'professional')->where('locked', false)->get();
        $subsidiaries = Subsidiary::all();
        return view('admin.calendars.edit', ['calendarService' => $calendarService, 'services' => $services, 'professionals' => $professionals, 'subsidiaries' => $subsidiaries, 'date' => $calendar->date, 'blocks' => $blocksAvailable]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::find($request->service_id);
        $oldCalendar = DB::table('calendar_service')->where('id', $request->pivot)->first();
        $blocks = ['1000', '1015', '1030', '1045', '1100', '1115', '1130', '1145', '1200',
                    '1215', '1230', '1245', '1300', '1315', '1330', '1345', '1400', '1415',
                    '1430', '1445', '1500', '1515', '1530', '1545', '1600', '1615', '1630',
                    '1645', '1700', '1715', '1730', '1745', '1800', '1815', '1830', '1845',
                    '1900', '1915', '1930', '1945', '2000'];

        $indexEndBlock = array_search($request->start_block, $blocks) + $service->blocks;
        $endBlockService = $blocks[($indexEndBlock > count($blocks)) ? count($blocks) - 1 : $indexEndBlock];

        $startBlock = array_search($oldCalendar->start_block, $blocks);
        $endBlock = array_search($oldCalendar->end_block, $blocks);
        $busyBlocks = array_slice($blocks, $startBlock, $endBlock - $startBlock);

        // En caso que los horarios cambien, borrar los horarios viejos borrar esos bloques
        if ($oldCalendar->start_block !== $request->start_block || $oldCalendar->end_block !== $endBlockService)  {
            foreach ($busyBlocks as $block) {
                Block::where('calendar_id', $oldCalendar->calendar_id)->where('block', $block)->where('subsidiary_id', $oldCalendar->subsidiary_id)->delete();
            }
        }


        $calendar = Calendar::firstOrCreate(['date' => date('Y-m-d', strtotime($request->date))]);
        $calendar->services()->updateExistingPivot($request->service_id, [
            'client_name' => $request->client_name,
            'client_surname' => $request->client_surname,
            'zip_code' => $request->zip_code,
            'notes' => $request->notes,
            'client_phone' => $request->client_phone,
            'start_block' => $request->start_block,
            'end_block' => $endBlockService,
            'professional_id' => $request->professional_id,
            'subsidiary_id' => $request->subsidiary_id
        ]);

        $startBlock = array_search($request->start_block, $blocks);
        $endBlock = array_search($endBlockService, $blocks);
        $busyBlocks = array_slice($blocks, $startBlock, $endBlock - $startBlock);
        
        foreach ($busyBlocks as $block) {
            $newBlock = new Block;
            $newBlock->block = $block;
            $newBlock->calendar_id = $calendar->id;
            $newBlock->subsidiary_id = $request->subsidiary_id;
            $newBlock->save();
        }
        return redirect()->route('calendars.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function destroy($idCalendar)
    {
        $calendar = DB::table('calendar_service')->where('id', $idCalendar)->first(['calendar_id', 'start_block', 'end_block']);
        $blocks = ['1000', '1030', '1100', '1130', '1200', '1230', '1300', '1330', '1600', '1630', '1700', '1730' ,'1800', '1830', '1900', '1930', '2000'];
        $blocksToRelease = collect([]);
        $startBlock = array_search($calendar->start_block, $blocks);
        $endBlock = array_search($calendar->end_block, $blocks);
        $busyBlocks = array_slice($blocks, $startBlock, $endBlock - $startBlock);
        foreach ($busyBlocks as $block) {
            DB::table('blocks')->where('calendar_id', $calendar->calendar_id)->where('block', $block)->delete();
        }
        DB::table('calendar_service')->where('id', $idCalendar)->delete();
        return response()->json(['result' => 'La cita fue removida exitosamente']);
    }
}
