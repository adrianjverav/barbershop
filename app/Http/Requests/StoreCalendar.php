<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreCalendar extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->type == 'admin' || Auth::user()->type == 'professional') {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_id' => 'required|exists:services,id',
            'client_name' => 'required',
            'client_surname' => 'required',
            'zip_code' => 'required|numeric',
            'notes' => 'required',
            'client_phone' => 'required',
            'professional_id' => 'required|exists:users,id',
            'start_block' => 'required',
            // 'end_block' => 'required',
            'subsidiary_id' => 'required|exists:subsidiaries,id',
        ];
    }
}
