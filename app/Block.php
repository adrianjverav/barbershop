<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $table = 'blocks';
    protected $fillable = ['block'];

    // Relaciones

    // un bloque pertenece a un calendario
    public function calendar(){
    	return $this->belongsToMany('App\Calendar');
    }

    // Uno o muchos bloques pertencen a una ciudad
    public function subsidiary()
    {
    	return $this->belongsToMany('App\Subsidiary');
    }
}
