<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';
    protected $fillable = ['service', 'blocks'];

    // RELACIONES --------------------

    // Un servicio puede estar en muchos días
    public function calendars()
    {
    	return $this->belongsToMany('App\Calendar');
    }

    // Un servicio es prestado por un user (professional)
    public function professional()
    {
    	return $this->belongsTo('App\User');
    }

    
}
