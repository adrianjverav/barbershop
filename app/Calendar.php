<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Calendar extends Model
{
    protected $table = 'calendars';
    protected $fillable = ['date', 'full'];

    // RELACIONES ------------------------

    // Un calendario tiene muchos servicios
    public function services()
    {
    	return $this->belongsToMany('App\Service', 'calendar_service', 'calendar_id', 'service_id')->withPivot('id', 'client_name', 'client_surname', 'zip_code', 'client_phone', 'notes', 'start_block', 'end_block', 'professional_id', 'subsidiary_id');
    }

    // Un calendario tiene muchos bloques
    public function blocks()
    {
    	return $this->hasMany('App\Block');
    }

    public static function getAllClients()
    {
        /*return DB::table('calendar_service')
                        ->select('client_name', 'client_surname', 'zip_code', 'client_phone')
                        ->groupBy('client_name', 'client_surname', 'zip_code', 'client_phone')->get();*/
        return DB::table('calendar_service')->orderBy('calendar_id')->get();
    }
}
