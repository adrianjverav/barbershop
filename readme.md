4## Pasos para ejecutar el proyecto

1. git clone https://bitbucket.org/adrianjvera21/barbershop/src
2. Ejecutar
```bash
    cd barbershop
```
3. Ejecutar
```bash
   composer update && npm install
```
4. Crear una base de dato con el nombre de preferencia y credenciales deseadas.
5. Crear un archivo de nombre .env en la raíz del proyecto siguiendo la estructura de .env.example
6. En el archivo .env colocar el nombre de la base de datos y sus credenciales
7. Ejectutar 
```bash
   php artisan migrate
```
(si existe un error, revisar la configuración de la base de datos)
8. Ejecutar
```bash
php artisan db:seed
```
9. Ejecutar
```bash
npm run dev
```
10. Ejecutar
```bash
php artisan serve --port=9090
```
11. Abrir localhost:9090


## DISFRUTAR
