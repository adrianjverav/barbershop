<?php

use Illuminate\Database\Seeder;

class SubsidiariesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subsidiaries')->insert([
        	'location' => 'Madrid'
        ]);

        DB::table('subsidiaries')->insert([
        	'location' => 'Sevilla'
        ]);

        DB::table('subsidiaries')->insert([
        	'location' => 'Barcelona'
        ]);

        DB::table('subsidiaries')->insert([
        	'location' => 'Cadíz'
        ]);
    }
}
