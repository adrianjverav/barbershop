<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $email = '@barbershop.com';

        DB::table('users')->insert([
        	'name' => 'Adrián',
			'surname' => 'Vera',
			'phone' => '0414-8539892',
			'type' => 'admin',
			'locked' => false,
			'email' => 'adrian' . $email,
			'password' => Hash::make('123456')
        ]);

        DB::table('users')->insert([
            'name' => 'Media',
            'surname' => 'Master',
            'phone' => '+1 (254) 224-5055',
            'type' => 'admin',
            'locked' => false,
            'email' => 'master' . $email,
            'password' => Hash::make('123456')
        ]);

        for ($i=0; $i < 3; $i++) {
        	DB::table('users')->insert([
        		'name' => $name = $faker->firstName,
				'surname' => $surname = $faker->lastName,
				'phone' => $faker->e164PhoneNumber,
				'type' => 'professional',
				'locked' => $faker->randomElement($array = array(true, false)),
				'email' => formatterEmail($name . $surname . $email),
				'password' => Hash::make('123456')
        	]);
        }
    }
}
