<?php

use Illuminate\Database\Seeder;

class CalendarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('calendars')->insert([
        	'date' => '2018-01-01',
        	'full' => false
        ]);

        DB::table('calendars')->insert([
        	'date' => '2018-01-03',
        	'full' => false
        ]);

        DB::table('calendars')->insert([
        	'date' => '2018-01-07',
        	'full' => false
        ]);

        DB::table('calendars')->insert([
        	'date' => '2018-01-10',
        	'full' => true
        ]);

        DB::table('calendars')->insert([
        	'date' => '2018-01-25',
        	'full' => false
        ]);
    }
}
