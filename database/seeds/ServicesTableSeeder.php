<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
        	'service' => "Micropigmentación Cejas Diseño",
            'blocks' => 1
        ]);

        DB::table('services')->insert([
        	'service' => "Micropigmentacion Repaso Mensual",
            'blocks' => 4
        ]);

        DB::table('services')->insert([
        	'service' => "Micropigmentación Repaso Anual",
            'blocks' => 5
        ]);

        DB::table('services')->insert([
        	'service' => "Micropigmentación Cejas Acabado",
            'blocks' => 2
        ]);

        DB::table('services')->insert([
        	'service' => "Micropigmentacion Cejas",
            'blocks' => 1
        ]);

        DB::table('services')->insert([
        	'service' => "Micropigmentacion Ojos",
            'blocks' => 4
        ]);

        DB::table('services')->insert([
        	'service' => "Micropigmentacion Labios",
            'blocks' => 3
        ]);
        
    }
}
