<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CaledarsServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_service', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calendar_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->integer('professional_id')->unsigned();
            $table->string('client_name');
            $table->string('client_surname');
            $table->string('zip_code', 10);
            $table->string('notes')->nullable();
            $table->string('client_phone');
            $table->enum('start_block', [
                '1000', '1015', '1030', '1045', '1100', '1115', '1130', '1145', '1200',
                '1215', '1230', '1245', '1300', '1315', '1330', '1345', '1400', '1415',
                '1430', '1445', '1500', '1515', '1530', '1545', '1600', '1615', '1630',
                '1645', '1700', '1715', '1730', '1745', '1800', '1815', '1830', '1845',
                '1900', '1915', '1930', '1945', '2000'
            ]);
            $table->enum('end_block', [
                '1000', '1015', '1030', '1045', '1100', '1115', '1130', '1145', '1200',
                '1215', '1230', '1245', '1300', '1315', '1330', '1345', '1400', '1415',
                '1430', '1445', '1500', '1515', '1530', '1545', '1600', '1615', '1630',
                '1645', '1700', '1715', '1730', '1745', '1800', '1815', '1830', '1845',
                '1900', '1915', '1930', '1945', '2000'
            ]);
            $table->foreign('calendar_id')->references('id')->on('calendars')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('professional_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendars_services');
    }
}
