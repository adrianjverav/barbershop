import {url} from '../app'

new Vue({
	el: '#subsidiaries',
	methods: {
		deleteSubsidiary: function (id) {
			this.$http.delete(`${url}subsidiaries/${id}`).then(res => {
				alert(res.body.result);
				location.reload();
			})
		}
	}
});