import {url} from '../app'

new Vue({
	el: '#services',
	methods: {
		deleteService: function (id) {
			this.$http.delete(`${url}services/${id}`).then(res => {
				alert(res.body.result);
				location.reload();
			})
		}
	}
});