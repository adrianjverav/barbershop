import {url} from "../app";

new Vue({
	el: '#professionals',
	methods: {
		deleteProfessional: function (id) {
			this.$http.delete(`${url}professionals/${id}`).then(res => {
				alert(res.body.result);
				location.reload();
			})
		},
		changeLockProfessional: function (id) {
			this.$http.put(`${url}professionals/change-lock-professional/${id}`).then(res => {
				alert(res.body.result);
				location.reload();
			})
		}
	}
});