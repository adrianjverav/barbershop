window._ = require('lodash');

try {
    window.$ = window.jQuery = require('jquery');

    window.Popper = Popper;
    require('bootstrap')
    require('datatables.net')
    require('bootstrap-datepicker')
    require('bootstrap-datepicker/js/locales/bootstrap-datepicker.es')
    require('fullcalendar')
    require('fullcalendar/dist/locale/es.js')
    require('axios')
    window.Vue = require('vue');
    window.VueResource = require('vue-resource')
    Vue.use(VueResource)
    Vue.config.debug = true;
    Vue.config.devtools = true;
} catch (e) {}

import Popper from 'popper.js/dist/umd/popper.js';

// let token = document.head.querySelector('meta[name="csrf-token"]');
// window.Vue.http.headers.common['X-CSRF-TOKEN'] = token.content;
// window.Vue.http.headers.common['Content-Type'] = 'application/json'
// window.Vue.http.headers.common['Access-Control-Allow-Origin'] = '*'
// window.Vue.http.headers.common['Accept'] = 'application/json, text/plain, */*'
// window.Vue.http.headers.common['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin'
