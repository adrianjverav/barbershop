import {url, urlWeb} from './app'

var idCalendar = 0;

$(document).ready(function() {
    $('#calendar').fullCalendar({
    	theme: false, 
    	header: {
            left: 'prev,next today',
            center: 'title',
            right: 'prevYear, nextYear'
        }, 
        events: {
        	url: `${url}calendars/dates`,
	        color: '#216ae0',
	    	textColor: 'white'
        },
        eventClick: function(calEvent, jsEvent, view) {
            $('#client').val(calEvent.client)
            $('#professional').val(calEvent.professional)
            $('#blocks').val(calEvent.blocks)
            $('#service').val(calEvent.service)
            $('#client_phone').val(calEvent.client_phone)
            $('#subsidiary').val(calEvent.subsidiary)

          	document.querySelector('#whatsapp').setAttribute('href', `https://api.whatsapp.com/send?phone=${calEvent.client_phone.replace(/[^A-Z0-9]/ig, "")}`)

            if (calEvent.notes) {
           		$('#notes').val(calEvent.notes)
            }
            $('#showDetailsModal').modal('show')
            idCalendar = calEvent.id
            $('#idCalendar').val(idCalendar)
            document.querySelector('#edit-button').setAttribute('href', `${urlWeb}calendars/edit/${idCalendar}`)
       }
    })
});

new Vue({
	el: '#form-create',
	data: {
		datepicker: '',
		professional_id: '',
		subsidiary_id: '',
		available: []
	},
	methods: {
		getAvailableBlocks: function () {
			var blocks = new Array
			this.available = []
			this.$http.get(`${url}calendars/check-availability/${this.datepicker}/${this.professional_id}/${this.subsidiary_id}`).then(res => {
					res.body.blocks.forEach(block => {
						blocks.push(block)
					})
					this.available = blocks;
				});
		}		
	},
	filters: {
		hour: function (block) {
			let hour = new Array;
			hour.push(block.substr(0, 2))
			hour.push(block.substr(2, 4))
			return hour.join(':')
		}
	}
})

new Vue({
	el: '#deleteCalendar',
	methods: {
		deleteCalendar: function () {
			this.$http.delete(`${url}calendars/${idCalendar}`).then(res => {
				alert(res.body.result)
				location.reload()
			})
		}
	}
})