// const url = 'http://citas.centroesteticamaytegarrido.com/api/';
// const urlWeb = 'http://citas.centroesteticamaytegarrido.com/';
const url = 'http://barber-shop.dev/api/';
const urlWeb = 'http://barber-shop.dev/';
export { url, urlWeb };

require('./bootstrap');
require('./sb-admin');
require('./calendar');
require('./ajax/professionals');
require('./ajax/services');
require('./ajax/subsidiaries');


$('#dataTable').DataTable({
    ordering: true,
    language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    select: {
        style: require('datatables.net-bs4')
    }
});

$('body').tooltip({selector: '[data-toggle="tooltip"]'});