<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ env('name') }}</title>
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
    @include('partials.navbar')
    <div class="content-wrapper">
        <div class="container-fluid">
            <ol class="breadcrumb">
                @yield('breadcrumbs')
                @yield('options')
            </ol>
            @yield('content')
        </div>
        @include('partials.footer')
    </div>
    <script src="{{ asset('/js/app.js') }}"></script>
    @yield('scrips')
</body>
</html>

