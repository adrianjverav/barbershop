@extends('layouts.auth')

@section('content')
    <div class="card-header">Inicio de sesión</div>
    <div class="card-body">
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email">Dirección de email</label>
                <input class="form-control" type="email" name="email" value="{{ old('email') }}" placeholder="Ingresa tu email" required autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password">Contraseña</label>
                <input class="form-control" type="password" name="password" placeholder="Ingresa tu contraseña" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="form-check-input" type="checkbox"> Recordar contraseña</label>
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-block">Entrar</button>
        </form>
        <div class="text-center">
            {{-- <a class="d-block small mt-3" href="register.html">Register an Account</a> --}}
            <a class="d-block small" href="{{ route('password.request') }}">¿Olvidaste tu contraseña?</a>
        </div>
    </div>
@endsection
