@extends('layouts.app')

@section('breadcrumbs')
	<li class="breadcrumb-item">
	  	<a href="{{ route('home') }}">Inicio</a>
	</li>
	<li class="breadcrumb-item active">
	  	Cambio de contraseña
	</li>
@endsection

@section('content')
	<form role="form" method="POST" action="{{ route('users.store-new-password') }}">
		{{ csrf_field() }}
		{{ method_field('PUT') }}

        <div class="form-group">
            <label for="password">Nueva contraseña</label>
            <input type="password" class="form-control" name="password" required>
            @if ($errors->has('password'))
	        	<span class="help-block">
	        		<strong>{{ $errors->first('password') }}</strong>
	        	</span>
	        @endif
        </div>
        <div class="form-group">
            <label for="password_confirmation">Confirme la contraseña</label>
            <input type="password" class="form-control" name="password_confirmation" required>
            @if ($errors->has('password_confirmation'))
	        	<span class="help-block">
	        		<strong>{{ $errors->first('password_confirmation') }}</strong>
	        	</span>
	        @endif
		</div>

		<button type="submit" class="btn btn-success">Guardar</button>
	</form>
@endsection