<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Inicio">
        <a class="nav-link" href="{{ route('calendars.index') }}">
            <i class="fa fa-calendar" aria-hidden="true"></i>
            <span class="nav-link-text">Calendario</span>
        </a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Crear turno">
        <a class="nav-link" href="{{ route('calendars.create') }}">
            <i class="fa fa-plus" aria-hidden="true"></i>
            <span class="nav-link-text">Crear turno</span>
        </a>
    </li>
    @if (Auth::user()->type === 'admin')
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Profesionales">
            <a class="nav-link" href="{{ route('professionals.index') }}">
                <i class="fa fa-users" aria-hidden="true"></i>
                <span class="nav-link-text">Profesionales</span>
            </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Servicios">
            <a class="nav-link" href="{{ route('services.index') }}">
                <i class="fa fa-archive" aria-hidden="true"></i>
                <span class="nav-link-text">Servicios</span>
            </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Sedes">
            <a class="nav-link" href="{{ route('subsidiaries.index') }}">
                <i class="fa fa-building" aria-hidden="true"></i>
                <span class="nav-link-text">Sedes</span>
            </a>
        </li>
    @endif
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Clientes">
        <a class="nav-link" href="{{ route('clients') }}">
            <i class="fa fa-user" aria-hidden="true"></i>
            <span class="nav-link-text">Clientes</span>
        </a>
    </li>
    @if (Auth::user()->type === 'admin')
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Cambio de contraseña">
            <a class="nav-link" href="{{ route('users.change-password') }}">
                <i class="fa fa-key" aria-hidden="true"></i>
                <span class="nav-link-text">Cambiar la contraseña</span>
            </a>
        </li>
    @endif
    <li><center><img src="https://i.imgur.com/bmKKBC6.png" width="200"/></center></li>
</ul>
