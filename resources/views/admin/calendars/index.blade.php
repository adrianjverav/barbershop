@extends('layouts.app')

@section('breadcrumbs')
	<li class="breadcrumb-item">
	  	<a href="{{ route('home') }}">Inicio</a>
	</li>
	<li class="breadcrumb-item active">
	  	Calendario
	</li>
@endsection

@section('content')
    <div id="calendar"></div>
    @include('admin.calendars.show-modal')
    @include('admin.calendars.delete-modal')    
@endsection