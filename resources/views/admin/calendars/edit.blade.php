@extends('layouts.app')

@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('home') }}">Inicio</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('calendars.index') }}">Calendario</a>
    </li>
    <li class="breadcrumb-item active">
        Edición de turno
    </li>
@endsection

@section('options')
    <li class="float-right">
        <a href="{{ URL::previous() }}">
            <i class="fa fa-table" aria-hidden="true"></i>
            Volver al listado</a>
        </li>
@endsection

@section('content')
    <form role="form" action="{{ route('calendars.update', [$calendarService->id]) }}" method="POST" id="form-create">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <div class="row">
            <div class="col-8">
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control" name="client_name" value="{{ $calendarService->client_name }}" required>
                    @if ($errors->has('client_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('client_name') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <label>Apellido</label>
                    <input type="text" class="form-control" name="client_surname" value="{{ $calendarService->client_surname }}" required>
                    @if ($errors->has('client_surname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('client_surname') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <label>Código postal</label>
                    <input type="text" class="form-control" name="zip_code" value="{{ $calendarService->zip_code }}" required>
                    @if ($errors->has('zip_code'))
                    <span class="help-block">
                        <strong>{{ $errors->first('zip_code') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <label>Teléfono</label>
                    <input type="text" class="form-control" name="client_phone" value="{{ $calendarService->client_phone }}" required>
                    @if ($errors->has('client_phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('client_phone') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <label>Notas</label>
                    <input type="text" class="form-control" name="notes" value="{{ $calendarService->notes }}" required>
                    @if ($errors->has('notes'))
                    <span class="help-block">
                        <strong>{{ $errors->first('notes') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="col-4 mt-4">
                <div class="form-group">
                    <select class="form-control" name="service_id" required>
                        <option selected>Selecciona el servicio</option>
                        @foreach ($services as $service)
                            <option value="{{ $service->id }}" {{ ($calendarService->service_id === $service->id) ? 'selected' : '' }}>
                                {{ $service->service }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <select class="form-control" name="professional_id" required>
                        <option selected>Selecciona el profesional</option>
                        @foreach ($professionals as $professional)
                            <option value="{{ $professional->id }}" {{ ($calendarService->professional_id === $professional->id) ? 'selected' : ''}}>
                                {{ $professional->name }} {{ $professional->surname }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <select class="form-control" name="subsidiary_id" required>
                        <option selected>Selecciona la sede</option>
                        @foreach ($subsidiaries as $subsidiary)
                            <option value="{{ $subsidiary->id }}" {{ ($calendarService->subsidiary_id === $subsidiary->id) ? 'selected' : '' }}>
                                {{ $subsidiary->location }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Fecha de cita</label>
                    <input type="text" class="form-control" name="date" value="{{ date('d-m-Y', strtotime($date)) }}" readonly>
                    @if ($errors->has('date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('date') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <select class="form-control" name="start_block" required>
                        {{-- <option selected>Hora de inicio</option> --}}
                        <option value="{{ $calendarService->start_block }}">{{ formatterBlock($calendarService->start_block) }}</option>
                        @foreach ($blocks as $block)
                            <option value="{{ $block }}" {{ ($calendarService->start_block === $block) ? 'selected' : '' }}>
                                {{ formatterBlock($block) }}
                            </option>
                        @endforeach
                    </select>
                </div>

                {{-- <div class="form-group">
                    <select class="form-control" name="end_block" required>
                        <option selected>Hora de fin</option>
                        @foreach ($blocks as $block)
                            <option value="{{ $block }}" {{ ($calendar->end_block === $block) ? 'selected' : '' }}>
                                {{ formatterBlock($block) }}
                            </option>
                        @endforeach
                    </select>
                </div> --}}
            </div>
        </div>        
        <input type="hidden" name="pivot" value="{{ $calendarService->id }}">
        <button type="submit" class="btn btn-success btn-sm">Guardar</button>
    </form>
@endsection