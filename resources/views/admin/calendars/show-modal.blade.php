<!-- Modal -->
<div class="modal fade" id="showDetailsModal" tabindex="-1" role="dialog" aria-labelledby="showDetailsModalLabel" aria-hidden="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detalles de la cita</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="client">Cliente</label>
                            <input type="text" class="form-control" id="client" name="client" placeholder="Datos de cliente" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="professional">Profesional</label>
                            <input type="text" class="form-control" id="professional" name="professional" placeholder="Datos de profesional" readonly>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="blocks">Horario</label>
                            <input type="text" class="form-control" id="blocks" name="blocks" placeholder="Horario de atención" readonly>
                        </div>
                        <div class="form-group col-md-6">

                            <label for="notes">Notas</label>
                            <input type="text" class="form-control" id="notes" name="notes" placeholder="Notas de la cita" readonly>
                        </div>                        
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="subsidiary">Sede</label>
                            <input type="text" class="form-control" id="subsidiary" name="subsidiary" placeholder="Sede" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="client_phone">Teléfono de cliente</label>
                            <a href="#" id="whatsapp" target="_blank">
                                <span>
                                    <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                </span>
                            </a>
                            <input type="text" class="form-control" id="client_phone" name="client_phone" placeholder="Teléfono de cliente" readonly>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="service">Servicio</label>
                            <input type="text" class="form-control" id="service" name="service" placeholder="Servicio" readonly>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <a id="edit-button" class="btn btn-primary">Editar</a>
                        <button type="button" class="btn btn-danger"  data-dismiss="modal" data-toggle="modal" data-target="#deleteCalendar">
                            Borrar
                        </button>
                    </div>
                    <input type="hidden" id="idCalendar" name="idCalendar">
                </div>
            </div>
        </div>
    </div>
</div>
