@extends('layouts.app')

@section('breadcrumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('home') }}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">
        Clientes
    </li>
@endsection

@section('content')
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Lista de clientes</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>ZIP</th>
                            <th>Inicio</th>
                            <th>Fin</th>
                            <th>Profesional</th>
                            <th>Teléfono</th>
                        </tr>
                    </thead>
                    <tbody id="clients">
                        @foreach ($clients as $client)
                            <tr>
                                <td> {{ date('d-m-Y', strtotime(\App\Calendar::find($client->calendar_id)->date)) }} </td>
                                <td> {{ $client->client_name }} </td>
                                <td> {{ $client->client_surname }} </td>
                                <td> {{ $client->zip_code }} </td>
                                <td> {{ formatterBlock($client->start_block) }} </td>
                                <td> {{ formatterBlock($client->end_block) }} </td>
                                <td> {{ \App\User::find($client->professional_id)->name . ' ' .  \App\User::find($client->professional_id)->surname}}</td>
                                <td>
                                    {{ $client->client_phone }}
                                    <a href="https://api.whatsapp.com/api/send?phone={{ formatterEmail($client->client_phone) }}" target="_blank">
                                        <span>
                                            <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection