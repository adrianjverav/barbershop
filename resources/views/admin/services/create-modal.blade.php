<!-- Modal -->
<div class="modal fade" id="createService" tabindex="-1" role="dialog" aria-labelledby="createService" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Creación de un servicio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form" action="{{ route('services.store') }}" method="POST">
                    {{ method_field('POST') }}
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Nombre de servicio</label>
                        <input type="text" class="form-control" name="service" value="{{ old('service')}}" required>
                        @if ($errors->has('service'))
                            <span class="help-block">
                                <strong>{{ $errors->first('service') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <select class="form-control" name="blocks" required>
                            <option selected value>Cantidad de tiempo para el servicio</option>
                            <option value="1">15 minutos</option>
                            <option value="2">30 minutos</option>
                            <option value="3">45 minutos</option>
                            <option value="4">60 minutos</option>
                            <option value="5">75 minutos</option>
                            <option value="6">90 minutos</option>
                            <option value="7">105 minutos</option>
                            <option value="8">120 minutos</option>
                            <option value="9">135 minutos</option>
                            <option value="10">150 minutos</option>
                            <option value="11">165 minutos</option>
                            <option value="12">180 minutos</option>
                            <option value="13">195 minutos</option>
                            <option value="14">210 minutos</option>
                            <option value="15">225 minutos</option>
                            <option value="16">240 minutos</option>
                        </select>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>