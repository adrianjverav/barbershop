@extends('layouts.app')

@section('breadcrumbs')
	<li class="breadcrumb-item">
	  	<a href="{{ route('home') }}">Inicio</a>
	</li>
	<li class="breadcrumb-item active">
	  	Servicios
	</li>
@endsection

@section('options')
    <li class="float-right">
        <a href="#" data-toggle="modal" data-target="#createService">
            <i class="fa fa-plus" aria-hidden="true"></i>
            Crear servicio
        </a>
    </li>
    @include('admin.services.create-modal')
@endsection

@section('content')
	<div class="card mb-3">
	    <div class="card-header">
	        <i class="fa fa-table"></i> Lista de servicios</div>
	    <div class="card-body">
	        <div class="table-responsive">
	            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	                <thead>
	                    <tr>
	                        <th>Servicio</th>
	                        <th>Duración</th>
	                        <th>Opciones</th>
	                    </tr>
	                </thead>
	                <tbody id="services">
	                	@foreach ($services as $service)
	                    	<tr>
	                    		<td>{{ $service->service }}</td>
	                    		<td>{{ $service->blocks * 15 }} minutos</td>
	                    		<td>
	                    			{{-- Editar --}}
	                    			<span data-target="#editService{{ $service->id }}" data-toggle="modal">
	                    				<a href="#" class="btn btn-simple btn-primary btn-icon" data-toggle="tooltip" data-placement="top" title="Editar servicio">
	                    					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
	                    				</a>
	                    			</span>
	                    			@include('admin.services.edit-modal', ['service' => $service, 'blocks' => $blocks])
	                    			{{-- Eliminar --}}
	                    			<span data-target="#deleteService{{ $service->id }}" data-toggle="modal">
	                    				<a href="#" class="btn btn-simple btn-danger btn-icon" data-toggle="tooltip" data-placement="top" title="Eliminar servicio">
	                    				    <i class="fa fa-trash" aria-hidden="true"></i>
	                    				</a>
	                    			</span>
	                    			@include('admin.services.delete-modal', ['service' => $service])
	                    		</td>
	                    	</tr>
	                    @endforeach
	                </tbody>
	            </table>
	        </div>
	    </div>
	</div>
@endsection