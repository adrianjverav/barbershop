<!-- Modal -->
<div class="modal fade" id="editService{{ $service->id }}" tabindex="-1" role="dialog" aria-labelledby="editService" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar servicio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form" action="{{ route('services.update', [$service->id]) }}" method="POST">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Nombre de servicio</label>
                        <input type="text" class="form-control" name="service" value="{{ $service->service }}" required>
                        @if ($errors->has('service'))
                            <span class="help-block">
                                <strong>{{ $errors->first('service') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <select name="blocks" class="form-control">
                            <option selected value>Seleccione la duración</option>
                            @foreach ($blocks as $block)
                                <option value="{{ $block }}" {{ ($block === $service->blocks) ? 'selected' : ''}}>
                                    {{ $block * 15 }} minutos
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success btn-sm">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>