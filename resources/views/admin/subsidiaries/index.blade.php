@extends('layouts.app')

@section('breadcrumbs')
	<li class="breadcrumb-item">
	  	<a href="{{ route('home') }}">Inicio</a>
	</li>
	<li class="breadcrumb-item active">
	  	Sedes
	</li>
@endsection

@section('options')
    <li class="float-right">
        <a href="#" data-toggle="modal" data-target="#createSubsidiary">
            <i class="fa fa-plus" aria-hidden="true"></i>
            Crear sede
        </a>
    </li>
    @include('admin.subsidiaries.create-modal')
@endsection

@section('content')
	<div class="card mb-3">
	    <div class="card-header">
	        <i class="fa fa-table"></i> Lista de sedes</div>
	    <div class="card-body">
	        <div class="table-responsive">
	            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	                <thead>
	                    <tr>
	                        <th>Ubicación</th>
	                        <th>Opciones</th>
	                    </tr>
	                </thead>
	                <tbody id="subsidiaries">
	                	@foreach ($subsidiaries as $subsidiary)
	                    	<tr>
	                    		<td>{{ $subsidiary->location }}</td>
	                    		<td>
	                    			{{-- Editar --}}
	                    			<span data-target="#editSubsidiary{{ $subsidiary->id }}" data-toggle="modal">
	                    				<a href="#" class="btn btn-simple btn-primary btn-icon" data-toggle="tooltip" data-placement="top" title="Editar sede">
	                    					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
	                    				</a>
	                    			</span>
	                    			@include('admin.subsidiaries.edit-modal', ['subsidiary' => $subsidiary])
	                    			{{-- Eliminar --}}
	                    			<span data-target="#deleteSubsidiary{{ $subsidiary->id }}" data-toggle="modal">
	                    				<a href="#" class="btn btn-simple btn-danger btn-icon" data-toggle="tooltip" data-placement="top" title="Eliminar sede">
	                    				    <i class="fa fa-trash" aria-hidden="true"></i>
	                    				</a>
	                    			</span>
	                    			@include('admin.subsidiaries.delete-modal', ['subsidiary' => $subsidiary])
	                    		</td>
	                    	</tr>
	                    @endforeach
	                </tbody>
	            </table>
	        </div>
	    </div>
	</div>
@endsection