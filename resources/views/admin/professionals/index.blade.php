@extends('layouts.app')

@section('breadcrumbs')
	<li class="breadcrumb-item">
	  	<a href="{{ route('home') }}">Inicio</a>
	</li>
	<li class="breadcrumb-item active">
	  	Profesionales
	</li>
@endsection

@section('options')
    <li class="float-right">
        <a href="{{ route('professionals.create') }}">
            <i class="fa fa-plus" aria-hidden="true"></i>
            Crear profesional
        </a>
    </li>
@endsection

@section('content')
	<div class="card mb-3">
	    <div class="card-header">
	        <i class="fa fa-table"></i> Lista de profesionales</div>
	    <div class="card-body">
	        <div class="table-responsive">
	            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	                <thead>
	                    <tr>
	                        <th>Nombre</th>
	                        <th>Apellido</th>
	                        <th>Teléfono</th>
	                        <th>Opciones</th>
	                    </tr>
	                </thead>
	                <tbody id="professionals">
	                	@foreach ($professionals as $professional)
	                    	<tr>
	                    		<td>{{ $professional->name }}</td>
	                    		<td>{{ $professional->surname }}</td>
	                    		<td>
	                    			{{ $professional->phone }}
	                    			<a href="https://api.whatsapp.com/api/send?phone={{ formatterEmail($professional->phone) }}" target="_blank">
	                    				<span>
	                    					<i class="fa fa-whatsapp" aria-hidden="true"></i>
	                    				</span>
	                    			</a>
	                    		</td>
	                    		<td>
	                    			@if (!$professional->locked)
	                    				{{-- Bloquear --}}
	                    				<a href="#" class="btn btn-simple btn-warning btn-icon" data-toggle="tooltip" data-placement="top" title="Bloquear profesional" @click.prevent="changeLockProfessional({{ $professional->id }})">
	                    					<i class="fa fa-lock" aria-hidden="true"></i>	
	                    				</a>
									@else
	                    				{{-- Desbloquea --}}
	                    				<a href="#" class="btn btn-simple btn-success btn-icon" data-toggle="tooltip" data-placement="top" title="Desbloqueaer profesional" @click.prevent="changeLockProfessional({{ $professional->id }})">
	                    					<i class="fa fa-unlock-alt" aria-hidden="true"></i>
	                    				</a>
	                    			@endif

	                    			{{-- Editar --}}
	                    			<a href="{{ route('professionals.edit', [$professional->id]) }}" class="btn btn-simple btn-primary btn-icon" data-toggle="tooltip" data-placement="top" title="Editar profesional">
	                    				<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
	                    			</a>
	                    			{{-- Eliminar --}}
	                    			<span data-target="#deleteProfessional{{ $professional->id }}" data-toggle="modal">
	                    				<a href="#" class="btn btn-simple btn-danger btn-icon" data-toggle="tooltip" data-placement="top" title="Eliminar">
	                    				    <i class="fa fa-trash" aria-hidden="true"></i>
	                    				</a>
	                    			</span>
	                    			@include('admin.professionals.delete-modal', ['professional' => $professional])
	                    		</td>
	                    	</tr>
	                    @endforeach
	                </tbody>
	            </table>
	        </div>
	    </div>
	</div>
@endsection