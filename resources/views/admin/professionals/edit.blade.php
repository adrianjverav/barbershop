@extends('layouts.app')

@section('breadcrumbs')
	<li class="breadcrumb-item">
	  	<a href="{{ route('home') }}">Inicio</a>
	</li>
	<li class="breadcrumb-item">
	  	<a href="{{ route('professionals.index') }}">Profesionales</a>
	</li>
	<li class="breadcrumb-item active">
	  	Edición de profesional
	</li>
@endsection

@section('options')
	<li class="float-right">
		<a href="{{ URL::previous() }}">
			<i class="fa fa-table" aria-hidden="true"></i>
			Volver al listado</a>
		</li>
@endsection

@section('content')
	<form role="form" action="{{ route('professionals.update', [$professional->id]) }}" method="POST">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
	    <div class="form-group">
	        <label>Nombre</label>
	        <input type="text" class="form-control" name="name" value="{{ $professional->name }}" required>
	        @if ($errors->has('name'))
	        	<span class="help-block">
	        		<strong>{{ $errors->first('name') }}</strong>
	        	</span>
	        @endif
	    </div>
	    <div class="form-group">
	        <label>Apellido</label>
	        <input type="text" class="form-control" name="surname" value="{{ $professional->surname }}" required>
	        @if ($errors->has('surname'))
	        	<span class="help-block">
	        		<strong>{{ $errors->first('surname') }}</strong>
	        	</span>
	        @endif
	    </div>
	    <div class="form-group">
	        <label>Teléfono</label>
	        <input type="text" class="form-control" name="phone" value="{{ $professional->phone }}" required>
	        @if ($errors->has('phone'))
	        	<span class="help-block">
	        		<strong>{{ $errors->first('phone') }}</strong>
	        	</span>
	        @endif
	    </div>
	    <div class="form-group">
	        <label>Correo eléctronico</label>
	        <input type="email" class="form-control" name="email" value="{{ $professional->email }}" required>
	        @if ($errors->has('email'))
	        	<span class="help-block">
	        		<strong>{{ $errors->first('email') }}</strong>
	        	</span>
	        @endif
	    </div>
	    {{-- <div class="form-group">
	        <label>Contraseña</label>
	        <input type="text" class="form-control" name="password" value="{{ old('password')}}">
	        @if ($errors->has('password'))
	        	<span class="help-block">
	        		<strong>{{ $errors->first('password') }}</strong>
	        	</span>
	        @endif
	    </div> --}}
	    <button class="btn btn-primary" type="submit">Guardar</button>
	</form>
@endsection